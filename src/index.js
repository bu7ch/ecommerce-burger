const express = require("express");
const app = express();
const session =  require("express-session");

const router = require("./routes/basicRoutes");
const port = 5000;
// CFG
const mysql = require("mysql");
const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database :'Ecomm_db',
  socketPath: "/Applications/MAMP/tmp/mysql/mysql.sock",
});
db.connect((err) => {
  if (err) throw err;
  console.log("Connecté à la base de données MySQL");
});

app.use(express.urlencoded({extended: true}));
app.use(session({secret:"burger"}))
app.use(express.static("public"));
app.set("views", "./views");
app.set("view engine", "pug");



app.use("/", router);

app.listen(port, () => console.log("App is ruunning on port " + port));
